import React from 'react';


const Badges = ({badges}) =>

    <div className="panel panel-default">
        <div className="panel-heading"><h4>Badges</h4></div>
        <div className="panel-body">
            {badges.map((badge, i) => {
                return(
                    <div key={i}>
                        <h4>{badge.shortName}</h4>
                        <p><img src={badge.image100px}/></p>
                        <p>{badge.description}</p>
                        <p>Earned {badge.timeAchieved}</p>
                        <p>Last on {badge.dateTime}</p>
                    </div>
                )
            })}
        </div>
    </div>

export default Badges