import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';
import LifetimeStats from './LifeTimeStats';
import dummyData from './dummyData';
import Badges from './Badges';
import TimeSeriesBarChart from './TimeSeriesBarChart'
import Friends from './Friends';



class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = dummyData
    }

    fetchFitBitData(url, fitbitToken, stateKey) {
        axios({
            method: 'get',
            url: url,
            headers: {'Authorization': 'Bearer ' + fitbitToken},
            mode: 'cors'
        }).then(response => {
            console.log(response);
            this.setState({[stateKey]: response.data})
        }).catch(error => console.log(error));
    }

    componentDidMount() {
        if (window.location.hash) {
            let fitbitToken = window.location.hash.slice(1).split('&')[0].replace("access_token=", "");

            this.setState({loggedIn: true});

            this.fetchFitBitData('https://api.fitbit.com/1/user/-/profile.json', fitbitToken,
                'user');
            this.fetchFitBitData('https://api.fitbit.com/1/user/-/activities.json', fitbitToken,
                'lifetimeStats');
            this.fetchFitBitData('https://api.fitbit.com/1/user/-/badges.json', fitbitToken,
                'badges');
            this.fetchFitBitData('https://api.fitbit.com/1/user/-/activities/steps/date/today/1m.json',
                fitbitToken, 'steps')
            this.fetchFitBitData('https://api.fitbit.com/1/user/-/activities/distance/date/today/1m.json',
                fitbitToken, 'distance')
            this.fetchFitBitData('https://api.fitbit.com/1/user/-/friends/leaderboard.json',
                fitbitToken, 'friends')

        }


    }


    render() {
        return (
            <div className="container">
                <header className="text-center">
                    <span className="pull-right">{this.state.user.user.fullName}</span>
                    <h1 className="page-header">React Fit</h1>
                    <p className="lead">Your personal fitness dashboard</p>
                </header>
                {!this.state.loggedIn &&
                <div className="row text-center">
                    <a href="https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=228QDZ&redirect_uri=http%3A%2F%localhost%3A3000&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800">
                        Log In with Fitbit</a>
                </div>
                }

                <div className="row">
                    <div className="col-lg-3">
                        <LifetimeStats lifetimeStats={this.state.lifetimeStats}/>
                        <Badges badges={this.state.badges.badges}/>

                    </div>

                    <div className="col-lg-6">
                        <TimeSeriesBarChart data={this.state.steps["activities-steps"]} title="Steps" yMax={8000} />

                        <TimeSeriesBarChart data={this.state.distance["activities-distance"]} title="Distance (KM)" yMax={6} />


                    </div>


                    <div className="col-lg-2 col-lg-offset-1">
                        <div className="panel panel-default">
                            <Friends friends={this.state.friends.friends} />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
        <Dashboard />,
        document.body.appendChild(document.createElement('div')),
    )
})

// https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=22942C&redirect_uri=http%3A%2F%2Fexample.com%2Ffitbit_auth&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800


// http://localhost:3000/#access_token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1UVg5Nk0iLCJhdWQiOiIyMjhRRFoiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyc29jIHJzZXQgcmFjdCBybG9jIHJ3ZWkgcmhyIHJudXQgcnBybyByc2xlIiwiZXhwIjoxNTA0NjAzNDI3LCJpYXQiOjE1MDM5OTg2Mjd9.Qsk4Nqu2L7VeoG5tpLrasKHXfQk3tYp3FWs0Q_DljDU&user_id=5QX96M&scope=sleep+settings+nutrition+activity+social+heartrate+profile+weight+location&token_type=Bearer&expires_in=604800